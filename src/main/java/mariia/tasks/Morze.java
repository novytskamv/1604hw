package mariia.tasks;

import java.util.HashSet;
import java.util.Set;

public class Morze {

  static String morze [] = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};

  static String alphabet [] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

  static Set <String> wordsSet = new HashSet<>();


  public int countUnique(String[] words){
    for(int i =0; i< words.length; i++) {
      String morzeWords = "";
      for (int j = 0; j < words[i].length(); j++) {
        for (int a = 0; a < Morze.alphabet.length; a++) {
          if (Morze.alphabet[a].toCharArray()[0] == words[i].toCharArray()[j]) {
            morzeWords += Morze.morze[a];
          }
        }
      }
      wordsSet.add(morzeWords);
    }
  return wordsSet.size();
  }

  public static void main(String[] args) {

//    String words [] = {"gin", "zen", "gig", "msg"};
//    System.out.println(Morze.countUnique(words));


  }

}

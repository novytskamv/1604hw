package mariia.tasks;

public class JewelsCalc {

  private String j;
  private String s;
  protected int price;

  protected static final String name = "Beauty Jewel";
  static final String maker = "Tiffany";

  public int calcJewels(String j, String s){
    int count = 0;
    for(int i =0 ; i< j.toCharArray().length; i++) {
      for (int k = 0; k< s.toCharArray().length; k++){
        if(j.toCharArray()[i] == s.toCharArray()[k] && j.toCharArray()[i] != ' '){
          count++;
        }
      }
    }
    return count;
  }

  public int calcStones(String j, String s){
    return s.length() - calcJewels(j, s);
  }

  public void calcPrice(String j, String s){
    price = calcJewels(j, s)*1000 + calcStones(j,s)*10;
  }

}

package mariia.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MorzeCalcTest {

  @InjectMocks
  Morze morze = new Morze();

  @Mock
  Morze morze1;

  @DisplayName("Calc Unique")
  @Test
  public void testCalcJUnique() {
    when(morze1.countUnique(new String[]{"gin", "zen", "gig", "gin"})).thenReturn(2);
    assertEquals(morze1.countUnique(new String[]{"gin", "zen", "gig", "gin"}), 2);
  }



}

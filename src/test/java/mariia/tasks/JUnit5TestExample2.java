package mariia.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class JUnit5TestExample2 {

  @DisplayName("Test on Calc Jewels")
  @Test
  void testCalcJewels1(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    assertEquals(jewelsCalc.calcJewels("aA","aAAbbbb"),3);
  }

  @DisplayName("Test on Calc Jewels")
  @Test
  void testCalcJewels2(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    assertEquals( jewelsCalc.calcJewels("   ","   "), 0 );
  }

  @DisplayName("Test on Calc Jewels")
  @Test
  void testCalcJewels3(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    assertEquals( jewelsCalc.calcJewels("z","zzzzzzzzz"), 9 );
  }

  @DisplayName("Test on Calc Stones")
  @Test
  void testCalcStones1(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    assert( jewelsCalc.calcStones("aA","aAAbbbb") == 4 );
  }

  @DisplayName("Test on Calc Stones")
  @Test
  void testCalcStones2(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    assert( jewelsCalc.calcStones("z","ZZ") == 2 );
  }

  @DisplayName("Test to check Null")
  @Test
  void testOnArraysNull(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    assertThrows(NullPointerException.class, ()->{jewelsCalc.calcJewels(null, "xf");});
  }

  @DisplayName("Test on protected fields")
  @Test
  void testOnProdectedfields(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    System.out.println(jewelsCalc.name);
  }

  @DisplayName("Test on package private fields")
  @Test
  void testOnPrivatefields(){
    JewelsCalc jewelsCalc = new JewelsCalc();
    System.out.println(jewelsCalc.maker);
  }


}

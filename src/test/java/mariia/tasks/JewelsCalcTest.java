package mariia.tasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class JewelsCalcTest {

  @InjectMocks
  JewelsCalc jewelsCalc = new JewelsCalc();

  @Mock
  JewelsCalc jewelsCalc1;

  @DisplayName("Calc Jewels")
  @Test
  public void testCalcJewels() {
    when(jewelsCalc1.calcJewels("aA", "aAAvfchb")).thenReturn(3);
    assertEquals(jewelsCalc1.calcJewels("aA", "aAAvfchb"), 3);
  }

  @DisplayName("Calc Stones")
  @Test
  public void testCalcStones() {
    JewelsCalc jewelsCalc = Mockito.mock(JewelsCalc.class);
    when(jewelsCalc.calcStones("zz", "ZZ")).thenReturn(2);
    assertEquals(jewelsCalc.calcStones("zz", "ZZ"), 2);

  }

  @DisplayName("Calc Stones VERIFY")
  @Test
  public void testCalcStones2() {
    when(jewelsCalc1.calcStones("zz", "ZZ")).thenReturn(2);
    assertEquals(jewelsCalc1.calcStones("zz", "ZZ"), 2, 0.000001);

    verify(jewelsCalc1).calcStones("zz", "ZZ");
  }

  @DisplayName("Void Method Test")
  @Test
  public void whenPriceCalcCalledVerfied(){
    JewelsCalc jewelsCalc = mock(JewelsCalc.class);
    doNothing().when(jewelsCalc).calcPrice(isA(String.class), isA(String.class));
    jewelsCalc.calcPrice("zz", "ZZ");

    verify(jewelsCalc, times(1)).calcPrice("zz", "ZZ");
  }

}
